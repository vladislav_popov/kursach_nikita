from django import forms  # import ModelForm, Form

from . import models


class CreateUserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput, required=True)

    class Meta:
        model = models.User
        fields = ['username', 'email', 'bio', 'first_name', 'last_name']


class LoginForm(forms.Form):
    username = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput, required=True)


class CreatePostForm(forms.ModelForm):
    class Meta:
        model = models.Post
        fields = ['title', 'description', 'youtube_link', 'soundcloud_link', 'preview_text']


class CommentForm(forms.ModelForm):
    class Meta:
        model = models.Comment
        fields = ['text']
