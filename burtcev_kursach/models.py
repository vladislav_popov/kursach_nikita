from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone


class User(AbstractUser):
    bio = models.TextField(max_length=500, blank=True)

    def __str__(self):
        return f'{self.username}'


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True)
    title = models.TextField(max_length=100, blank=False, null=False)
    preview_text = models.TextField(max_length=100, blank=False, null=False)
    description = models.TextField(max_length=2000, blank=False, null=False)
    youtube_link = models.CharField(max_length=1000, blank=True, null=True)
    soundcloud_link = models.CharField(max_length=1000, blank=True, null=True)
    date_posted = models.DateTimeField(auto_now_add=True)

    @property
    def detail_link(self):
        return f'/view_post/{self.id}/'

    def __str__(self):
        return f'Post #{self.id} by {self.author}'


class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    post = models.ForeignKey(Post, on_delete=models.DO_NOTHING)
    text = models.TextField(max_length=300, blank=False, null=False)
    date_posted = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Comment #{self.id} by {self.author}'
