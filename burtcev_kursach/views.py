from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from .models import Post
from .forms import CreateUserForm, LoginForm, CreatePostForm, CommentForm


def create_user(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.set_password(form.cleaned_data['password'])
            user.save()
            messages.success(request, 'Now use your username and password to login')
            return HttpResponseRedirect('/login')
    else:
        form = CreateUserForm()
    return render(request, 'register.html', {'form': form})


def login_user(request):
    errors = ''
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/')
        else:
            errors = 'Bad credentials'
    form = LoginForm()
    user = request.user
    return render(request, 'login.html', {'form': form, 'user': user, 'errors': errors})


def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')


def create_post(request):
    if not request.user.is_authenticated:
        return HttpResponse('You\'re not logged in', status=403)
    else:
        if request.method == 'POST':
            form = CreatePostForm(request.POST)
            if form.is_valid():
                post = form.save(commit=False)
                post.author = request.user
                post.save()
                return HttpResponseRedirect(f'/view_post/{post.id}/')
            else:
                return HttpResponse(form.errors, status=404)
        else:
            form = CreatePostForm()
        return render(request, 'create_post.html', {'form': form})


def post_details(request, id):
    try:
        post = Post.objects.get(id=id)
    except Post.DoesNotExist:
        return HttpResponse('This post does not exists', status=404)
    create_comment_form = CommentForm()
    comments = post.comment_set.all().order_by('-id')
    user = request.user
    return render(request, 'post_details.html', {'post': post, 'comments': comments, 'user': user, 'comment_form': create_comment_form})


def make_comment(request, id):
    if not request.user.is_authenticated:
        return HttpResponse('You\'re not logged in', status=403)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            try:
                comment.post = Post.objects.get(id=id)
            except Post.DoesNotExist:
                return HttpResponse('error', status=400)
            comment.save()
            return HttpResponseRedirect(f'/view_post/{id}/')
        else:
            return HttpResponse(form.errors, status=400)


def main_page(request):
    all_posts = Post.objects.all().order_by('-id')
    return render(request, 'main_page.html', {'all_posts': all_posts, 'user': request.user})